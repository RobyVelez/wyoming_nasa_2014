\documentclass[10pt]{article}
\usepackage[tmargin=1in,bmargin=1in,lmargin=1in,rmargin=1in]{geometry}
%\usepackage{fontspec}
%\setmainfont{Times New Roman}
%This package allows you to include figures
\usepackage{graphicx}
\usepackage{threeparttable} %table environment that works well with footnotes
\usepackage{wrapfig}
\usepackage[usenames,dvipsnames]{color}
\usepackage{pdfpages}
\usepackage{url}

\usepackage{caption}
\usepackage{subcaption}

\newcommand{\todo}[1]{\textcolor{red}{[#1]}} %to make the ToDo Command
\newcommand{\done}[1]{\textcolor{blue}{[#1]}} %to make the Comment Command
\newcommand{\comment}[1]{\textcolor{magenta}{[#1]}} %to make the Comment Command
\newcommand{\jmccomment}[1]{\textcolor{OliveGreen}{[#1]}} %to make the Comment Command
\long\def\/*#1*/{} %block comments
%list of colors here: http://en.wikibooks.org/wiki/LaTeX/Colors 

\author{
    Clune, Jeff\\
    Assistant Professor, Department of Computer Science\\
    Director, Evolving Artificial Intelligence Lab\\
  University of Wyoming\\
%  1000 E University Ave\\
%  Laramie, WY 82071 US\\
  jeffclune@uwyo.edu
}
\title{Developing More Autonomous Space Exploration Robots via Modular Learning: Phase II}

\date{}

\begin{document}
  \maketitle
\newpage
  \begin{abstract}

Robots are invaluable tools for space exploration. They affordably collect difficult-to-obtain data without risking human lives. However, their current need for constant human supervision limits their potential. The ability to \emph{learn} would make them more autonomous and useful, which can be achieved through computational brain models called Artificial Neural Networks (ANNs). Knowledge in ANNs is encoded in the weights of connections between neurons. Learning modifies these connections, but because learned information in ANNs tends to be uniformly distributed, learning a new skill overwrites previously learned skills, a phenomenon called \emph{catastrophic forgetting}. We have recently demonstrated that catastrophic forgetting is mitigated when combining \emph{neuromodulated learning}, which can turn learning on and off in specific situations, with \emph{modular} ANNs. Modular ANNs contain clusters of highly-connected nodes that are sparsely connected to other clusters. These modules can encode different skills and learning can be turned on only in the relevant module, minimizing forgetting other tasks. In a Wyoming Space Grant (WSG) Graduate Research Fellowship, we identified three promising ways to further improve modular, modulated ANN learning: 
(1) Basing neuromodulation on diffusing chemical signals (neurotransmitters), which more effectively modulates learning in a dense module of neurons,  
(2) Identifying appropriate learning rates, and (3) Physically separating motor command output streams for different tasks. 
%In the later two we have found a large impact on modular skill acquisition. 
The graduate WSG was spent building and running experiments to identify these three promising techniques (and to rule out others). In this Faculty WSG, we propose to translate this preliminary work into a publication by rigorously testing the generality, robustness, statistical significance, and effect size of these three techniques for improving the ability of robots to learn new skills without forgetting previously learned skills. We will also extend this work to simulated and real robots. 
%These three elements show great promise in producing modular ANNs where the learning of one skill does not destroy another. We plan to continue to investigate and refine these elements in order to produce ANNs that can compartmentalize different skills and are more targeted during learning. Our experiments will incorporate both simulated and physical robots, evaluated for multiple tasks. Crucial is the comparison to results with previous methods.
%Overall, Our research will improve robot learning algorithms that can ultimately increase the autonomy and effectiveness of space exploration robots.
  
\end{abstract}
\newpage

%Project Narrative: Advance state of learning within robots in order to improve future robotic space missions.

\subsection*{Description of Proposed Project}

\subsubsection*{Motivation}

Robots like the Mars rovers allow us to investigate other planets, discover resources favorable to human colonization, and investigate the likelihood of life beyond Earth, all while being (relatively) affordable and eliminating risks to human life. However, these machines face immense challenges including the inability to communicate with Earth in real time and being rendered inoperable by things that a human operator could trivially fix, such as a stuck wheel or partially deployed solar panel. If instead robots could learn to solve new problems themselves they would be more autonomous and useful. 

\subsubsection*{Problem}

Learning is notoriously difficult in robots~\cite{floreano2008bio}. A leading means of giving robots the ability to learn is to equip them with Artificial Neural Network (ANN) controllers that learn~\cite{floreano2008bio}. ANNs are modeled on animal brains, and the function they perform depends on which neurons (nodes) are connected to each other, the strength of those connections, and whether they are inhibitory or excitatory. Those properties are refined by a simulated process of evolution, wherein the best ANNs from the previous generation are kept, randomly mutated, and then evaluated in the next generation~\cite{floreano2008bio}. Because these networks learn within their life (via Hebbian learning~\cite{hebb2005organization}), there is an evolutionary pressure to create better learners~\cite{floreano2008bio}. Typical Hebbian learning can be further improved via \emph{modulatory} neurons, which turn learning on or off in specific situations~\cite{soltoggio2008evolutionary}.

One drawback to evolving ANNs is that they are nearly always highly interconnected (i.e. non-modular), causing information to be uniformly distributed throughout the entire network~\cite{clune2013originModularity}. Thus, when weights are modified to learn a new task, any previously encoded knowledge is lost, a problem known as  \emph{catastrophic forgetting}~\cite{mccloskey1989catastrophic}. We recently showed that catastrophic forgetting is reduced in ANNs that are~\emph{modular}~\cite{ellefsenModularity}, meaning networks made up of clusters of highly-connected neurons that are sparsely coupled to surrounding clusters.  We discovered that modular networks can be evolved by imposing a cost for neural connections during evolution~\cite{clune2013originModularity}. Modularity helps mitigate forgetting because each module can encode a different skill and modulatory neurons can allow learning \emph{only} in a module dedicated to a particular skill without erasing knowledge in the rest of the network. While the results from that work show that ANNs with elevated modularity demonstrate increased learning and decreased catastrophic forgetting, the amount of modularity that developed within the ANNs was less than is needed to truly eliminate catastrophic forgetting~\cite{ellefsenModularity}. This proposal focuses on further increasing modularity in learning ANNs to substantially reduce catastrophic learning and, in turn, allow robots to learn many different skills without erasing previously acquired knowledge. Specifically, we focus on three techniques that our work to date has identified as promising ways to increase modular learning and thus reduce catastrophic forgetting. 

\begin{wrapfigure}{r}{0.6\textwidth}
\vspace{-15pt}
\centering
	\begin{subfigure}[b]{0.6\textwidth}
                \includegraphics[width=\textwidth,height =0.55\textwidth]{images/Fitnessdiffusion.png}    
                
        \end{subfigure}   
        \vspace{-25pt}     
        \caption{\footnotesize Allowing neuromodulatory signals to diffuse and thus affect multiple, nearby neurons at once significantly improves performance (p$<$0.05).}
                 \label{fig:diffusionHelps}
        \vspace{-10pt}
       
\end{wrapfigure}

\textbf{Technique 1--Diffusing Neurotransmitters:}
In our original work showing that modularity reduces catastrophic forgetting~\cite{ellefsenModularity}, neuromodulation involved one-to-one signals between modulatory and modulated neurons. Biological neurons, however, have the ability to release neuromodulatory chemicals (neurotransmitters) that can diffuse, and thus affect a group of nearby neurons. The focus of our previous WSG Graduate Research Fellowship proposal was to explore the advantages of diffusing neuromodulation versus standard one-to-one modulation. Our experiments reveal that diffusion improves overall performance and modularity, making it a promising new technique~(Fig.~\ref{fig:diffusionHelps}). However, while diffusion improved performance, contrary to our expectations, it did not cause a modular decomposition of the pattern-recognition problem. We conducted detailed analyzes of the ANNs to see what changes might instead produce such a modular decomposition. Those analyses led to the two following techniques.  

\begin{wrapfigure}{r}{0.25\textwidth}

%\begin{figure}
	\begin{subfigure}[b]{0.4\textwidth}
                \includegraphics[width=.65\textwidth,height =0.3\textwidth]{images/nn_full100Cropped.png}
                \vspace{-5pt}
                \caption{\scriptsize\raggedright{ANN with a high learning rate.}}
                \label{fig:nonReducedANN}
        \end{subfigure}
        ~
        \begin{subfigure}[b]{0.4\textwidth}
                \includegraphics[width=.65\textwidth,height=0.3\textwidth]{images/nn_full50Cropped.png}
                \vspace{-5pt}
                \caption{\scriptsize\raggedright{ANN with a low learning rate.}}
                \label{fig:reducedANN}  
        \end{subfigure}
%        \vspace{-20pt}
	\caption{\footnotesize Neural networks that learn at a high rate (top) can create a separate learning module (white bordered neurons), but don't functionally decompose the rest of the problem. Neural networks that learn at a low rate (bottom) also possess separate learning modules, but each learning module receives reward feedback from a particular season and delivers targeted learning towards the connections in the network that encode that season. F: fitness. M: modularity score. The network is algorithmically cut into modules (orange, blue, and green) following the standard procedure in~\cite{Newman2006}.}
  	\label{fig:modNetworks}   
\vspace{-10pt}
%\end{figure}
\end{wrapfigure}

\textbf{Technique 2--Lower Learning Rates:}
Fig.~\ref{fig:nonReducedANN} shows networks obtained by replicating~\cite{ellefsenModularity}, but with a different ANN on a more biologically realistic version of that paper's food-foraging problem. 
%The input set was simplified in order to solely focus on what factors are relevant to obtaining a proper modular split. 
%The topology changed to one that is more biological plausible. Aside from those variations the overall experiment remains the same.
%The networks are evolved to control simulated robots that must forage for food~\cite{ellefsenModularity}. 
For this task an artificial agent lives in a world with two seasons (summer and winter), and must learn to identify poisonous foods from non-poisonous foods. The seasons cycle and each has distinct food items. The agents must learn which foods are nutritious and toxic in both seasons, and are more fit if they retain that knowledge instead of relearning it anew each time. The ANNs have 5 inputs. From left to right: two for seeing the food, one indicating the season, and two providing summer and winter reward signals, which indicate whether food just eaten was nutritious or toxic. The networks have two outputs, one per season, which determine if a piece of food is eaten. The problem is modular and thus should produce distinct modules dedicated to identifying each season's food associations. The problem is purposefully simple to facilitate our understanding.

The networks shown in Fig.~\ref{fig:nonReducedANN} posses the same decomposition as those in our previous work~\cite{ellefsenModularity}. There is a central cluster of neurons that classify whether an object is food or poison, and a separate learning module (white bordered neuron) that is activated by both reward signals. If the network had separated summer and winter pattern-recognition then the reward signals would also have been separate instead of merging into a signal learning module. Even without a modular separation of the two pattern-recognition problems, this network still obtains high fitness. 
%We analyzed these networks and discovered that they were not retaining all the associations between seasons, and simply relearning after each switch. This relearning is due to the fact the the rate of weight modification is so high that the ANNs can change connections, and thereby skills, almost instantaneously. 
We experimented with the within-life learning rate and discovered that a much lower rate promotes separate pattern-recognition modules (Fig.~\ref{fig:reducedANN}) and reduced catastrophic forgetting. In Fig.~\ref{fig:reducedANN}, the reward signals of the network tie into separate learning modules, each targeting different parts of the network. We will investigate the robustness of this effect across different problem types and networks, as well as study precisely why it occurs. 
%When learning takes longerWith a reduced learning rate it takes longer to learn an association. Because of the time investment networks can no longer erase old knowledge to learn a new association, but must careful compartmentalize information to reduce any type of interference.

% While there is some segmentation of neurons into a food classification module and a reward module (Fig. ~\ref{fig:summerWinter}) there is no modular decomposition between summer and winter encodings. 


\textbf{Technique 3--Physically separating output channels:}
We also discovered that the number and geometric arrangement of outputs and inputs greatly affects evolved modularity. 
%Most of the modular structures within natural brains corresponds to different functionalities.
%As occurs in the brain, where motor  is physically separate, 
We discovered that changing the topology of the ANNs to incorporate two outputs, one for each season, made it more likely that networks  evolved separate pathways for summer and winter foods. Another topology change involved creating a more biological plausible input sensor, where the patterns for winter and summer foods are shown to the same visual retina, instead of having separate summer and winter inputs. We also added a sensor that indicates whether it is winter or summer. 
%eliminating separate input neurons for each season and instead having one visual sensor that recognizes feeding the food items for both seasons into the same input neurons.

%\todo{We already mentioned what follows...that's the low learning rate idea/technique}s
%The second realization came from the fact that many networks were not actually retaining skills between seasons, but rather were quickly learning the correct associations after a few food presentations. This relearning is due to the fact the the rate of weight modification is so large that the ANNs can change connections, and thereby skills, almost instantaneously. When we lowered the maximum rate at which weights could change we saw a dramatic drop in the amount of total relearning. With a reduced learning rate it takes longer to learn an association. Because of the time investment networks can no longer erase old knowledge to learn a new association, but must careful compartmentalize information to reduce any type of interference.


% to prevent Lastly we've observed that limiting how fast ANNs can modify weights during learning can vastly dictate whether they isolate skills and make slow progress toward each or simply wipe the slate clean during learning. If networks can alter connections from one extreme to another instantaneously, thereby switching from skill A to skill B instantaneously, there is no advantage to retain skill A when using skill B.

%Because networks no longer knew if the food they were receiving was from summer or winter we also add an input neuron that indicated the season. The change to the input neurons also reflects a realization that biological systems do not have separate input streams for each skill they must acquire.  

\subsubsection*{Proposed Action Plan}

\begin{wrapfigure}{r}{0.4\textwidth}
\vspace{-30pt}

	\begin{subfigure}[b]{0.42\textwidth}
\hspace{-10pt}
                \includegraphics[width=1.0125\textwidth,height =0.9\textwidth]{images/Kuka.png}
                \vspace{-9pt}
               
        \end{subfigure}
        \caption{\footnotesize The Kuka YouBot robot, on which we will experiment. Its omnidirectional platform is equipped with a 5-DOF arm and two-finger gripper. Video of robot: http://goo.gl/o0Vf4E}
                 \label{fig:kukaRobot}        
        \vspace{-10pt}
       
\end{wrapfigure}

In preliminary experiments, the above techniques have shown great promise at promoting modular skill learning and reducing catastrophic forgetting, but much research remains to be done. The goal of this proposal is to translate our preliminary successful results into a publication. To do that, we will test whether the results for the above techniques are robust across different types of problems, input sets, and network configurations. The first step is rigorously assessing the effect size and statistical significance of our results on the current, simple, diagnostic problems we described above. The second step is then to scale up the size of the ANN and the difficulty of the problem. To that end, we will experiment with simulated and real robots. The real robot we will use is the sophisticated (\$25,000) Kuka YouBot robot (Fig.~\ref{fig:kukaRobot}), which we recently purchased. We will have this robot learn to solve different tasks, such as recognizing and putting away different types of objects into different receptacles. Being able to demonstrate that our modular, modulated learning techniques work to reduce catastrophic forgetting on this sophisticated industrial robot will provide a real-world demonstration of the efficacy of our approach. It will also encourage future research, by ourselves and other research groups, into scaling these techniques up to more challenging problems that will aid NASA in its goal of creating more autonomous robots for scientific exploration.



%\todo{Describe what we really plan to do here. Scale experiments to harder problems that require larger networks, test on simulated and real robots, and translating this preliminary work into a publication by rigorously testing the generality, robustness, statistical significance, and effect size of the impact these three techniques have on improving the ability of robots to learn new skills without forgetting previously learned skills. 
%}


%To test the generality and robustness we will statistically compare our work to other results within the field of learning, and submit our findings to the rigors of peer review.

%Once we have built a level of understanding and success with the current artificial agent setup, we plan to move onto real robots. We must ensure that the results obtained in simulation transfer to the real world; a fact not always guaranteed. \comment{mention the robots we have? RV} Our ultimate product is a physical robot that can learn and retain multiple tasks during real world operation. These tasks will be molded after tasks faced by everyday exploratory robots such as the use of robotic arms equipped with tools, identification and collection of samples, and non-human directed locomotion in face of harsh terrain and possibly damage.

\subsubsection*{Conclusion}

Until robots can learn to function on their own, their true usefulness, both on Earth and in space exploration, will be greatly limited. While robots can currently learn one task, they struggle to learn multiple, different tasks because of the problem of catastrophic forgetting. 
%Improving the state of learning within robots is crucial to making these machines powerful tools in our quest to explore the unknown. 
Our research group is steadily improving things on this front. We have demonstrated both how to evolve modular neural networks~\cite{clune2013originModularity} and that such modularity reduces catastrophic forgetting when combined with neuromodulatory learning~\cite{ellefsenModularity}. The next step is moving beyond the simple proof of concept in ~\cite{ellefsenModularity} to a more practical algorithm that consistently enables robots to learn multiple, different tasks. The research proposed herein moves us closer to that goal by taking the next step in that direction. 

\subsection*{Relationship to NASA Goals}

The lion's share of data collection on the planets, asteroids, and comets in our solar system in the decades ahead will be performed by robots. All of those goals are catalyzed by more autonomous, effective robots. 

\subsection*{Products} We anticipate submitting a peer-reviewed publication by the project end date (see below). We will present the work in the Computer Science Research Seminar on the UW campus, and submit it for presentation at both the Genetic \& Evolutionary Computation Conference and the Artificial Life Conference. We also plan to submit an NSF proposal on the next major phase of this work, which will involve scaling learning up to many different tasks, and learning more complex challenges on sophisticated robots. 
 
\subsection*{Timeline} July 2015-July 2016. The first nine months will be spent designing and conducting experiments, analyzing data, and forming the outline of the publication. We will begin writing the publication around March, 2016 and should submit it by the project close around July 2016.  Our preparation of an NSF proposal will depend on the deadlines for relevant programs. 


\bibliographystyle{plain}
\bibliography{modularRobotLearning}
\newpage
\section*{Budget}

\newcommand{\tab}[1]{\hspace{.2\textwidth}\rlap{#1}}
\begin{table}[h!]
	\centering
	\begin{threeparttable}[h!]
		   \label{tab:test2}
		    \begin{tabular}{  l  l }
		    \caption{Budget Request}
		     Labor: & \\ \hline
		     \tab{}Twelve Months Support for Ph.D. Graduate Student & ~\$21,207\\ 
		      \tab{}Advisor Hours (4 hr/wk $\times$ 52 wk $\times$ \$55/hr) & ~\$11,440\\ 
		     
		     \\	
		     Other Costs: & \\ \hline
		     \tab{}Mount Moran Usage Cost (200hr $\times$ \$2.10 hr/node $\times$ 100 node) & ~\$42,000\\
		     \tab{}Computation Unit for UW Supercomputer (Mount Moran) & ~\$6,958\\ 
		     \\
		     Total Need: & \textbf{ \$81,605} \\
		    \hline
		     \\
		     Cost Share Deductions: & \\ \hline
		    \tab{}Mount Moran Usage Cost\footnotemark[1] (200hr $\times$ \$2.10 hr/node $\times$ 100 node) & -\$42,000\\
		     \tab{}Covering Ph.D. Support over $20,000$\footnotemark[2]  & -\$1,207\\

		     \tab{}Mount Moran Computation Unit\footnotemark[2] & -\$6,958\\
		     %\tab{}Hexapod & -\$6,500 \\ 
		     \tab{}Advisor Hours  & -\$11,440\\
		     %\tab{}Publication Fees - GECCO 2015  & -\$500 \\ 
		     \\
		     
		     Cost Share: & \textbf{-\$61,605} \\
		      \hline
		      \\
		     \textbf{Amount Request:} & \textbf{ \$20,000} \\
		     \hline
		    \end{tabular}
		      \begin{tablenotes}
		      		\item[1] Estimate value of resource. Use is free for UW members.
    				\item[2] Funding Source - Professor Clune's Startup Research Grant
 		      \end{tablenotes} 
	\end{threeparttable}
\end{table}

All of the money requested from the Wyoming Space Grant will go to fund Ph.D. student Roby Velez for 1 year. Velez is performing the research and is advised by Dr. Clune. The cost to support Velez is over the \$20,000 maximum allowed for this proposal, and the difference will come from Clune's startup funds as \$1,207 in cost share. 

The remaining costs are all cost share. \$42,000 worth of computation time on the Mt. Moran supercomputer is included, although that resources is free for UW researchers (more on that below). \$11,440 in advisor time is donated by Dr. Clune. The remaining costs (a computation unit to augment Mt. Moran) will be funded through Dr. Clune's startup funds.

This project will heavily rely on the computing power provided by the Mount Moran supercomputer. Mount Moran is an invaluable research tool that is in high demand. \$6,958 from Professor Clune's Startup Research Grant will be spent to purchase an additional node, thereby allowing more experiments to be run at the same time. These nodes are powerful, professional computation devices. Aside from the physical device their cost includes support, maintenance, cooling, bandwidth, and storage. While an additional node will be a great asset to this project, it is also a long-term investment in research at the University of Wyoming. It will be in use long after the completion of this project, and will be available to any UW researcher when the Evolving AI Lab is not performing experiments on the computer.

Since we are part of the University of Wyoming, there is no charge to use Mount Moran. However, because it is such an invaluable resource for this project, we will include the estimated cost for its use. We expect to need 200 hours of computing with 100 nodes. At \$2.10 hours per node the estimated cost of use is \$42,000. This value is factored into our cost share.

Finally, Professor Clune will commit 208 hours of advising spread over the year. At an hourly rate of \$55/hr, the total value of his consultation is priced at \$11,440. The hours committed will encompass one-on-one meetings, daily correspondence via email, and consultation on publications, presentations, and other materials showcasing the results of this project. 

\subsection{Current and Pending Funding}

The PI has not received any funding for this project, nor does he have any pending funding for it. His student (Velez) did receive a Wyoming Space Grant Graduate Student Fellowship for the initial research that led to this proposal. That fellowship ends just before this proposed project would start. 

\includepdf[pages={1,2}]{cluneCV.pdf}
  \end{document}
