\documentclass[12pt]{article}
\usepackage[tmargin=1in,bmargin=1in,lmargin=1in,rmargin=1in]{geometry}
%\usepackage{fontspec}
%\setmainfont{Times New Roman}
%This package allows you to include figures
\usepackage{graphicx}
\usepackage{threeparttable} %table environment that works well with footnotes
\usepackage{wrapfig}
\usepackage[usenames,dvipsnames]{color}
\usepackage{pdfpages}
\usepackage{url}

\usepackage{caption}
\usepackage{subcaption}


\newcommand{\todo}[1]{\textcolor{red}{[#1]}} %to make the ToDo Command
\newcommand{\done}[1]{\textcolor{blue}{[#1]}} %to make the Comment Command
\newcommand{\comment}[1]{\textcolor{magenta}{[#1]}} %to make the Comment Command
\newcommand{\jmccomment}[1]{\textcolor{OliveGreen}{[#1]}} %to make the Comment Command
%list of colors here: http://en.wikibooks.org/wiki/LaTeX/Colors 

\author{
  Student: \\
  Velez, Roby\\
  University of Wyoming\\
  1000 E University Ave\\
  Laramie, WY 82071 US\\
  rvelez@uwyo.com
  \and
  Advisor: \\
  Clune, Jeff\\
  University of Wyoming\\
  1000 E University Ave\\
  Laramie, WY 82071 US\\
  jeffclune@uwyo.com
}
\title{Developing More Autonomous Space Exploration Robots via Modular Learning}


\begin{document}
  \maketitle
\newpage
  \begin{abstract}
  
Space exploration is greatly aided by using robots. They allow us to rapidly and economically collect valuable information without risking human lives. However, the need for frequent human supervision limits their potential. One way to improve their usefulness is to make them more autonomous through the endowment of learning abilities. Learning can be achieved through robot controllers made of computational brain models called Artificial Neural Networks (ANNs). Knowledge in ANNs is encoded in the patterns and weights of connections between neurons. Learning, which can be adjusted in specific situations via \emph{modulatory} neurons, occurs by modifying these connections. Since learned information in ANNs tends to be uniformly distributed, these modifications disrupt skills previously learned. This is known as \emph{catastrophic forgetting}. Recently, my advisor demonstrated that catastrophic forgetting is mitigated in \emph{modular} ANNs. Modular ANNs contain clusters of highly-connected nodes that are sparsely coupled to one another. These modules can encode different skills. When modulatory neurons target a \emph{specific} module for learning, forgetting is minimized because skills present in the rest of the network are unaffected. Recent research has shown that even slight increases in modularity improves learning by reducing forgetting. We propose to study a technique that should further increase modularity, and thus performance. Our idea is to base neuromodulation more closely on mechanisms found in biological brains. Specifically we plan to use modulatory neurons that produce diffusing chemical signals (neurotransmitters). A diffusing signal is more efficient at uniformly affecting all the neurons within a module, densely packed with nodes. To test this method we will evaluate the performance of simulated and physical robots, controlled with learning ANNs, on multiple tasks. We will then compare our results with previous methods. This work will improve robot learning algorithms that can ultimately increase the autonomy and effectiveness of space exploration robots.


\end{abstract}
\newpage
\section{Proposal}
\subsection{Improving Robot Learning via Modular Neural Networks}

Space exploration is one of the many great endeavors attempted by mankind. Aside from fueling our hunger for discovery, it pushes the limits of human innovation and leads to technological advances. Of the many tools at our disposal, one of the greatest is robots. Robots allow us to collect valuable information ranging from the habitability of other planets to the question of whether we are alone in the universe. Their deployment and operation also make it possible to tackle many challenges to space exploration, all without risking human life. That being said the full potential of robots is limited by their need for constant human supervision. One way to lessen the dependence on human operators, and extend robot's usefulness, is to make them more autonomous by endowing them the ability to engage in lifelong learning. 

%Learning with neuromodulation and modularity
One way to achieve learning in robots is with controllers made from Artificial Neural Networks (ANNs) that engage in learning through neuromodulation. These ANNs are obtained through an Evolutionary Algorithms (EAs), which are a search optimization techniques inspired by natural evolution~\cite{clune2013originModularity}. ANNs are computational models of neurons found in animal brains. Information in these networks is encoded in the patterns and weights of connections between neurons (nodes). Learning occurs by changing those weights, and can be further improved via \emph{modulatory} neurons, which can increase or decrease learning in specific situations. These ANNs tend to be highly interconnected, causing information to be uniformly distributed throughout the entire structure. When weights are modified to learn a new task, any previously encoded knowledge is lost, a problem known as  \emph{catastrophic forgetting}. 

My advisor recently showed that catastrophic forgetting is reduced in ANNs that are~\emph{modular}~\cite{ellefsenModularity}, meaning networks made up of clusters of highly-connected neurons that are sparsely coupled to surrounding clusters.  Modular networks can be evolved by imposing a cost for neural connections during evolution~\cite{clune2013originModularity}. Modularity helps mitigate forgetting because each module can encode a different skill. Modulatory neurons can allow learning \emph{only} in a module dedicated to a particular skill without erasing knowledge in the rest of the network. While the results from my advisor's work show that ANNs with elevated modularity demonstrate increased learning and decreased catastrophic forgetting, the amount of modularity that developed within the ANNs was less than expected~\cite{ellefsenModularity}.

Fig.~\ref{fig:summerWinter} shows networks evolved to control simulated robots that must forage for food~\cite{ellefsenModularity}. For this task an artificial agent lives in a world with two seasons (summer and winter) and must learn to identify poisonous foods from non-poisonous foods. The seasons cycle and each has distinct food items. The agents must learn the correct association for each season without those associations interfering with one another. The networks of the artificial agents contain 10 inputs corresponding to the summer foods, winter foods, and reward signals indicating poisonous or not. This is a modular problem and should produce distinct modules dedicated to identifying each season's food associations and a third module for processing the reward inputs. While there is some segmentation of neurons along the path leading from the reward signals (Fig. ~\ref{fig:summerWinter}) there is no modular decomposition of summer and winter inputs. 

In contrast, Fig.~\ref{fig:retina} shows \emph{non-learning} networks evolved for another modular task. In this pattern-recognition problem the networks are presented a stimulus of 8 inputs. The networks must recognize if a specific pattern (object) is present in both the left and right half of an 8-pixel retina. It is clear that the networks have decomposed the task into two distinct sub problems~(Fig.~\ref{fig:retina}). This type of \emph{functional} modularity would improve neural networks that learn, but did not emerge in~\cite{ellefsenModularity}. We propose to further increase modularity in learning neural networks to achieve reduce forgetting. 

\begin{wrapfigure}{r}{0.4\textwidth}
\vspace{-10pt}
%\begin{figure}
\centering
	\begin{subfigure}[b]{0.4\textwidth}
                \includegraphics[width=\textwidth,height =0.6\textwidth]{images/Summer_WinterNetwork.png}
                \vspace{-20pt}
                \caption{\raggedright{\textbf{Neural networks that learn.}}}
                \label{fig:summerWinter}
        \end{subfigure}
        ~
        \begin{subfigure}[b]{0.4\textwidth}
                \includegraphics[width=\textwidth,height=0.6\textwidth]{images/RetinaNetwork.png}
                \vspace{-20pt}
                \caption{\raggedright{\textbf{Non-learning networks.}}}
                \label{fig:retina}  
        \end{subfigure}
        \vspace{-20pt}
	\caption{\textbf{\footnotesize Neural networks that \emph{learn} currently can create a separate learning module (orange neurons), but don't functionally decompose the rest of the problem~\cite{ellefsenModularity}. Networks without learning currently can functionally decompose problems~\cite{clune2013originModularity}. We propose to make the learning networks in (a) have more problem decomposition like those in (b). Doing so should improve robot learning algorithms.}}
  	\label{fig:modNetworks}   
\vspace{-20pt}
%\end{figure}
\end{wrapfigure}

Neuromodulation in the previously mentioned work~\cite{ellefsenModularity} involves one-to-one signals between modulatory and target neurons. Real neurons make use of direct communication, but also have the ability to release neuromodulating chemicals (neurotransmitters) that can diffuse. We hypothesize that diffusing neurotransmitters can increase learning in modular networks because they have the ability to uniformly effect a cluster of neurons, turning learning on or off. 

We propose to test that hypothesis. The experiments will be conducted in ANNs that control legged robots evolved in simulation and then ported to the real world. The robots will have to learn to perform different tasks throughout their lives. While neurotransmitters have shown promise for evolving learning with simulated robots~\cite{kondo2007evolutionary}, their ability to improve learning in modular networks has never been tested. 

\subsection{Timeline and Implementation}

Our initial experiments will test the performance of diffusing neurotransmitters on the problems from~\cite{ellefsenModularity}. This phase of the project should span most of the Fall 2014 term. The next phase involves experimenting on legged robots, first in simulation then on physical robots. This will take up the Spring 2015 term and following summer.

Legged robots are a promising platform for NASA space rovers because they can explore more rugged terrain and can extricate themselves from situations where wheeled robots can become stuck or inoperable. Our simulations will focus on  environments with varied landscapes, noisy sensor readings, and servo malfunctions. These are  situations that exploratory robots encounter, meaning we need to test on them. They are also situations where learning can help with the need to adapt to such changes, so such situations should reveal the benefits of improved learning.

\subsection{Products}

The products of this work will include a paper and presentation submitted to GECCO, the top conference in the field of evolutionary algorithms, in 2015 (Madrid, Spain). I will follow that up with a longer paper in a top journal, such as \emph{IEEE Transactions on Evolutionary Computation}. I will also create videos documenting the research and post them to the Evolving AI Lab Youtube video channel, which has hundreds of subscribers and has produced viral science videos in the past. Additionally, I will present this work at seminar series at the University of Wyoming. Finally, there is the chance for press coverage of the work, as our lab's research is often covered by major national and international media outlets.

Aside from the relevance to NASA's goals and improved neural learning algorithms, this work will advance the field of robotics, which will have repercussions in many facets of research and human life. Robotics is a rapidly growing field, and the improving of it can have far reaching societal benefits. Among the gains will be improving robots that perform hazardous jobs such as bomb disposal, industrial applications, consumer products, and providing entertainment or assistance to the disabled and elderly. 

\subsection{Relationship to NASA Goals}

Sending robots to other planets is crucial in information gathering, but is also a costly operation that takes years to plan. The best way to get as much performance and utility from these machines, with the least cost, is by endowing them the ability to learn. To illustrate this point I cite the events that befell the Mars rover Spirit. 

Spirit is no longer operable and resides on the west side of Home Plate on the red planet. It became stuck in May 2009 due to its inability to free itself from a section of soft soil. Unable to move and orient toward sunlight it fell prey to the Mars winter. When Spirit became stuck, operators at JPL had a difficult time formulating a plan to free it. They could only use the information gathered from Spirit's sensors and their simulations were not completely relevant since the soil mechanics on Mars are different from those found on Earth. If Spirit had the ability to learn and adapt it could have assessed its predicament using data at hand, and formulated a plan tailored to that situation. In theory Spirt could still be roaming the surface of Mars, gathering data, like its twin Opportunity.

\bibliographystyle{plain}
\bibliography{modularRobotLearning}
\newpage
\section{Funding Request}

\newcommand{\tab}[1]{\hspace{.2\textwidth}\rlap{#1}}
\begin{table}[h!]
	\centering
	\begin{threeparttable}[h!]
		   \label{tab:test2}
		    \begin{tabular}{  l  l }
		    \caption{Budget Request}
		     Labor: & \\ \hline
		     \tab{}Supplemental Ph.D. Graduate Fellowship & ~\$9,815\\ 
		     \tab{}One Month Summer Support for Ph.D. Graduate Student & ~\$2,000\\ 
		     \\ 
		     Six Legged Robot Parts: & \\ \hline
		     \tab{}Power Supply & ~\$3,500\\
		     \tab{}MX 28T Chassis Servos & ~\$3,300 \\
		     \tab{}Custom Laser Cut Robot Chassis & ~\$350\\
		     \tab{}OnBoard Computer & ~\$320\\
		     \tab{}Assorted Cables for Power and Servo Communication & ~\$240\\
		     \tab{}AX-18 Neck Servos & ~\$190\\ 
		     \tab{}KINECT, Gyroscope, and Touch Sensors & ~\$160\\
		     \tab{}Gyroscope & ~\$125\\
		     %\tab{}Parts for Six Legged Mobile Robot (Hexapod) &\$8,185.00 \\ 
		     \\	
		     Other Costs: & \\ \hline
		     \tab{}Mount Moran Usage Cost (200hr $\times$ \$2.10 hr/node $\times$ 100 node) & ~\$42,000\\
		     \tab{}Computation Unit for UW Supercomputer (Mount Moran) & ~\$6,958\\ 
		      \tab{}Advisor Hours (3 hr/wk $\times$ 35 wk $\times$ \$50/hr) & ~\$5,250\\ 
		     \tab{}Travel - GECCO 2015 Conference (Madrid, Spain) & ~\$2,500\\ 
		     \tab{}Publication Fees - GECCO 2015 & ~\$500\\
		     \\
		     Total Need: & \textbf{ \$77,208} \\
		    \hline
		     \\
		     Cost Share Deductions: & \\ \hline
		    \tab{}Mount Moran Usage Cost\footnotemark[1] (200hr $\times$ \$2.10 hr/node $\times$ 100 node) & -\$42,000\\

		     \tab{}Mount Moran Computation Unit\footnotemark[2] & -\$6,958\\
		     %\tab{}Hexapod & -\$6,500 \\ 
		     \tab{}Advisor Hours  & -\$5,250\\
		     \tab{}Travel - GECCO 2015 Conference (Madrid, Spain)\footnotemark[2] & -\$2,500\\ 
		     \tab{}Publication Fees - GECCO 2015 & -\$500\\
		     %\tab{}Publication Fees - GECCO 2015  & -\$500 \\ 
		     \\
		     
		     Cost Share: & \textbf{-\$57,208} \\
		      \hline
		      \\
		     \textbf{Amount Request:} & \textbf{ \$20,000} \\
		     \hline
		    \end{tabular}
		      \begin{tablenotes}
		      		\item[1] Estimate value of resource. Use is free for UW members.
    				\item[2] Funding Source - Professor Clune's Startup Research Grant
 		      \end{tablenotes} 
	\end{threeparttable}
\end{table}

For this project I am requesting the full \$20,000 in funding. Of that amount, \$11,815 will go towards my stipend for the Spring 2015 term and one month of support for the following summer. I am currently funded through the Minority and Women Assistantship, which covers me through the Fall 2014 term. 

The remaining \$8,185 in funding will be used to purchase a six legged (hexapod) robot. The hexapod will be our testing platform and is an integral part of this project. We have already purchased a hexapod robot through the University of Wyoming Engineering Fund for Enrichment (UWEFE), but it is only a teaching tool. This additional research hexapod will be more advanced; possessing sensors necessary for this type of project, and built to withstand the rigors of experimentation. Its cost includes a professional grade power source, precision servos, custom robot chassis, onboard computer, and sophisticated sensors. 

The remaining costs (cost share) include \$2,500 to support travel expenses needed to attend and present this work at the 2015 GECCO conference in Madrid, Spain, which is the top conference in our field, \$500 for publication fees, \$6,958 for an additional supercomputer (Mount Moran) computation node, \$42,000 worth of computation time on the supercomputer, and \$5,250 in advisor time. The costs associated with the supercomputer and advisor time are described below. With the exception of the computing and advisor time, all of these items will be funded through Professor Clune's Startup Research Grant.

This project will heavily rely on the computing power provided by the Mount Moran supercomputer. Mount Moran is an invaluable research tool that is in high demand. \$6,958 from Professor Clune's Startup Research Grant will be spent to purchase an additional node, thereby allowing more experiments to be run at the same time. These nodes are powerful, professional computation devices. Aside from the physical device their cost includes support, maintenance, cooling, bandwidth, and storage. While an additional node will be a great asset to this project it is also an investment to research at the University of Wyoming. It will be in use long after the completion of this project.

Since we are part of the University of Wyoming there is no charge to use Mount Moran, but because it is such an invaluable resource for this project we will include the estimated cost for its use. We expect to need 200 hours of computing with 100 nodes. At \$2.10 hours per node the estimate cost of use is \$42,000. This value is factored into our cost share.

Finally, Professor Clune will commit 105 hours of advisement spread over the 2014/2015 academic year and subsequent summer. At an hourly rate of \$50/hr the total value of his consultation is priced at \$5,250. The hours committed will encompass one-on-one meetings, daily correspondence via email, and consultation on publications, presentations, and other materials showcasing the results of this project. This final expense brings the total cost share value to  \$57,208.

\subsection{Attached PDFs}

The following PDFs include:

\begin{itemize}

  \item Vita of Student
  \item Vita of Faculty Advisor
  \item Unofficial Transcript from University of Wyoming (Ph.D - Current)
  \item Unofficial Transcript from University of Sussex (MSc - 2011/2012)
  \item Unofficial Transcript from Swarthmore College (BS - 2005/2009)
  

\end{itemize}

\includepdf[pages={1,2}]{VelezCV.pdf}
\includepdf[pages={1,2}]{cluneCV.pdf}
\includepdf[pages={1,2}]{UnofficalTranscript.pdf}
\includepdf[pages={1}]{Velez_Sussex_UnofficialTranscript.pdf}
\includepdf[pages={1,2}]{Velez_Swat_Transcript.pdf}

  \end{document}
